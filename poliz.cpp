#include "poliz.h"

Poliz :: Poliz (int max_size)
{
    fre = 0;
}

void Poliz :: put_lex (Lex l)
{
    //p [fre] = l;
    p.push_back(l);
    fre++;
}

Lex & Poliz :: operator[] (int index)
{
   
    if (index > fre)
        throw runtime_error("POLIZ:indefinite element of array");
    else
        return p[index];
}

void Poliz :: print ()
{
    for (int i = 0; i < fre; i++)
    {    
        cout << i << " " << p[i];
    }
}