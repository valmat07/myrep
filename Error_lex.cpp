#include "Error_lex.h"

Syntax_error :: Syntax_error(Lex l, string miss_lex)
{
	message = "expected";
	message += " ";
	message += miss_lex;
	message += " ";
	message += "before";
	message += " ";
	message += l.get_name();
	num_str = l.get_str();
	num_col = l.get_col();
}

Semantic_error:: Semantic_error(Lex lx, string inf_er)
{
	message = lx.get_name();
	message += " ";
	message += inf_er;
	num_str = lx.get_str();
	num_col = lx.get_col();
	
}
Wrong_type::Wrong_type(const string tp, int n_st, int n_cl, type_of_lex tol)
{
	type = tp;
	lex_type = tol;
	num_col = n_cl;
	num_str = n_st;
}
Wrong_type::Wrong_type(int n_st, int n_cl, type_of_lex tol)
{
	lex_type = tol;
	num_col = n_cl;
	num_str = n_st;	
}
const string Wrong_type::Wrong_type :: what()
{	
	
	message = "Invalid type in ";
	message += type;
	message += " ";
	if (message != "")
	{	
		if (lex_type != LEX_NULL)
		{
			switch (lex_type)
			{
				case LEX_OR:
					message += "or";
					break;
				case LEX_AND:
					message += "and";
					break;
				case LEX_PLUS:
					message += "+";
					break;
				case LEX_MINUS:
					message += "-";
					break;
				case LEX_MULT:
					message += "*";
					break;
				case LEX_SLASH:
					message += "/";
					break;
			}
		}
	}
	else
	{
		message = "expression is not boolean";
	}
	cout << "in line " << num_str << ":" << num_col << " ";
	return message;
}
