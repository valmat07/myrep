#include "lex.h"
using namespace std;
Lex::Lex(type_of_lex t, int v, const char * buf, int s, int c): t_lex(t), v_lex(v), s_lex(s), c_lex(c)
{
	name = new char [strlen(buf)];
	strcpy(name, buf);
}
Lex::Lex(const string tmp, type_of_lex t, float v, const char * buf, int s, int c): t_lex(t), float_v_lex(v), s_lex(s), c_lex(c)
{
	name = new char [strlen(buf)];
	strcpy(name, buf);
	v_lex = 0;
}
ostream& operator << (ostream &s, Lex l)
{
	s << '(' << l.t_lex << ',' << l.v_lex << "); " << l.name << " ";
	switch(l.get_type())
	{
		case LEX_SEMICOLON: cout << ";";
							break;
		case LEX_ASSIGN:	cout << "=";
							break;
		case LEX_OR:
			cout << "or";
			break;
		case LEX_AND:
			cout << "and";
			break;
		case LEX_PLUS:
			cout << "+";
			break;
		case LEX_MINUS:
			cout << "-";
			break;
		case LEX_MULT:
			cout << "*";
			break;
		case LEX_SLASH:
			cout << "/";
			break;
		case LEX_MORE:	cout << ">";
						break;
		case LEX_EQ:	cout << "==";
						break; 

		case LEX_LESS:	cout << "<";
						break;
		case LEX_LEQ:	cout << "<="; 
						break;
		case LEX_NEQ:	cout << "!=";
						break;
		case LEX_GEQ:	cout << ">=";
						break;


		case POLIZ_LABEL:	cout << "LABEL";
							break;
		case POLIZ_ADDRESS: cout << "ADDRES";
							break;
		case POLIZ_GO: 	cout << "!" << " ";
						break;
		case POLIZ_FGO: cout << "!F" << " ";
						break;
	}
	cout << endl;
	//return s;
}
