#include "exec.h"
void Executer::execute ( Poliz & prog )
{
  stack <float> args;
  int  index = 0, size = prog.get_free();
  string str;
  float i, j, op1, op2	;
  while (index < size)
  {
    lex_type = prog[index];
    switch (lex_type.get_type ())
    {
    	case LEX_TRUE: case LEX_FALSE: case LEX_NUM: case POLIZ_ADDRESS: case POLIZ_LABEL:
        	args.push (lex_type.get_value());
        	break;

      	case LEX_STR:
      		str = lex_type.get_name();
      		break;
      	case LEX_ID:
        	i = lex_type.get_value ();
        	args.push (TID[i].get_value());
        	break;
        
        case LEX_FLOAT_NUM:
        	args.push (lex_type.get_float_value());

        	break;

      	case LEX_NOT:
        	args.push(!args.top());
        	args.pop();
        	break;

      	case LEX_OR:
        	op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push (op2 || op1);
        	break;

      	case LEX_AND:
       		op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push (op2 && op1);
        	break;

      	case POLIZ_GO:
        	index = args.top() - 1;
       	 	args.pop();
        	break;

      	case POLIZ_FGO:
        	i = args.top();
        	args.pop();
        	if (!args.top()) 
        		index = i - 1;
        	args.pop();
        	break;

      	case LEX_PRINT:
        	cout << args.top () << endl;
        	args.pop();
        	break;

      	case LEX_READ:
        {
          	int k;
          	i = args.top();
          	args.pop();
          	if ( TID[i].get_type() == LEX_INT || TID[i].get_type() == LEX_FLOAT)
          	{
           		cin >> k;
          	}
          	else
          	{
            	string bool_val;
            	bool b = true;
            	while (b)
            	{
	            	cin >> j;
	            	if (bool_val == "true")
	            	{
	            		k = 1;
	            		b = false;
	            	}
	            	else if (bool_val == "false")
	            	{
	             		b = false;
	                	k = 0;
	            	}
		        	else
		        		cout << "Error in input:true/false" << endl;
          	}
          }
          	TID[i].put_value(k);
          	break;
        }

      	case LEX_PLUS:
      		op1 = args.top();
      		args.pop();
      		op2 = args.top();
      		args.pop();
        	args.push (op1 + op2);
        	break;

      	case LEX_MULT:
      		op1 = args.top();
      		args.pop();
      		op2 = args.top();
      		args.pop();
        	args.push (op1 * op2);
        	break;

      	case LEX_MINUS:
        	op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push (op2 - op1);
        	break;

      	case LEX_SLASH:
        	op1 = args.top();
        	args.pop();
        	if (!op1)
        	{
          		op2 = args.top();
          		args.pop();
         		args.push(op2 / op1);
         		break;
        	}
        	else
         		throw runtime_error("POLIZ:divide by zero");

      	case LEX_EQ:
      		op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push ( op1 == op2 );
        	break;

      	case LEX_LESS:
        	op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push ( op2 < op1);
        	break;

      	case LEX_MORE:
        	op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push ( op2 > op1);
        	break;

      	case LEX_LEQ:
       		op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push ( op2 <= op1);
        	break;

      	case LEX_GEQ:
        	op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
        	args.push ( op2 >= op1);
        	break;

      	case LEX_NEQ:
        	op1 = args.top();
        	args.pop();
        	op2 = args.top();
        	args.pop();
       		args.push ( op2 != op1);
        	break;

      	case LEX_ASSIGN:
        	i = args.top();
        	args.pop();
        	j = args.top();
        	args.pop();
        	TID[j].put_value(i); 
        	break;
        case LEX_PRINTL:
        	cout<< str << endl;
        	str = "";
        	break;
     	default:
        	throw runtime_error("POLIZ: unexpected elem");
    }
    index++;
  };
}