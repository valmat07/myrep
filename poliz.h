#pragma once
#include "lex.h"
class Poliz
{
  vector<Lex> p;
  int size, fre;
public:
    Poliz (int max_size);
    void put_lex (Lex l);
    void put_lex (Lex l, int place) { p [place] = l; }
    void blank() 
    { 
        p.push_back(Lex(LEX_NULL)); 
        fre++; 
    }
    int get_free() { return fre; }
    Lex & operator[] (int index);
    void print ();
};