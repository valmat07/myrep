#pragma once
#include "lex.h"
class Syntax_error : public exception
{
private:
	string message;
	int num_str = 0, num_col = 0;
public:
	Syntax_error(Lex l, string miss_lex);
	int get_num_str(){return num_str;}
	int get_num_col(){return num_col;}
	string get_mes() const {return message;}
	string what() 
	{ 
		cout << "in line " << num_str << ":" << num_col << " ";
		return message;
	}
};
class Semantic_error :public exception
{
private:
	string message;
	int num_str, num_col;

public:
	Semantic_error(Lex lx, string inf_er);
	virtual const string what()
	{
		cout << "in line " << num_str << ":" << num_col << " ";
		return message;
	}
};

class Wrong_type :public exception
{
private:
	string type, message = "", oper;
	int num_str, num_col;
	type_of_lex lex_type = LEX_NULL;
public:
	Wrong_type(const string tp, int n_st = 1, int n_cl = 1, type_of_lex tol = LEX_NULL);
	Wrong_type(int n_st = 1, int n_cl = 1, type_of_lex tol = LEX_NULL);
	const string what();
};

