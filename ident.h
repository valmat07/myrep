#pragma once
#include "lex.h"

class Ident
{
	char* name;
	bool declare;
	type_of_lex type;
	bool assign;
	float value;
public:
	Ident();
	char* get_name() { return name; }
	void put_name(const char *n);
	bool get_declare() { return declare; }
	void put_declare() { declare = true; }
	type_of_lex get_type() { return type; }
	void put_type(type_of_lex t) { type = t; }
	bool get_assign() { return assign; }
	void put_assign() { assign = true; }
	float get_value() { return value; }
	void put_value(float v) { value = v; }
	void del_name () {delete [] name;}
};
class Tabl_ident
{
	Ident *p;
	int size;
	int top;
public:
	Tabl_ident(int max_size);
	Ident & operator[] (int k) { return p[k]; }
	int put(const char *buf);
};