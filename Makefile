PROG = task4
lex.o: lex.cpp lex.h
	g++ -std=c++11 -g -c $< -o $@

scanner.o: scanner.cpp scanner.h lex.o
	g++ -std=c++11 -g -c $< -o $@

ident.o: ident.cpp ident.h lex.o
	g++ -std=c++11 -g -c $< -o $@

parser.o: parser.cpp parser.h scanner.o ident.o lex.o	
	g++ -std=c++11 -g -c $< -o $@

Error_lex.o: Error_lex.cpp Error_lex.h lex.o
	g++ -std=c++11 -g -c $< -o $@

poliz.o: poliz.cpp poliz.h lex.o
	g++ -std=c++11 -g -c $< -o $@

exec.o:	exec.cpp exec.h lex.o poliz.o ident.o
		g++ -std=c++11 -g -c $< -o $@

task4: task4.cpp lex.o scanner.o ident.o parser.o Error_lex.o poliz.o exec.o
		g++  -std=c++11 task4.cpp lex.o scanner.o ident.o parser.o Error_lex.o poliz.o exec.o -o task4

clean:
	rm -f *.o $(PROG)

run:
	./$(PROG)
