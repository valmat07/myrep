#pragma once
#include "lex.h"
#include "scanner.h"
#include "ident.h"
#include "Error_lex.h"
#include "poliz.h"
class Parser
{
	Lex curr_lex;
	type_of_lex c_type;
	int c_val, count_cycle, count_if, N = 100, count = 0, count_break = 0;
	//Lex* tmp_lexs = nullptr;
	std::vector<Lex> tmp_lexs;
	Scanner scan;
	priority_queue<int> pl_4_break, tmp_lst;
	list<int> pl_4_cont; 
	stack <int> st_int;
	stack <type_of_lex> st_lex;

	/*first reads the types of variables and their,  
	and then begins to analyze the body of the program*/
	void Start();
	void Ident_type();
	void ident_variable();
	void ident_body_in_figure();
	void ident_body();
	void ident_expression();
	void expres1();
	void expres2();
	void pars_oper();
	void Check_declare(type_of_lex type);
	void check_id();
	void check_op();
	void check_not();
	void eq_type();
	void eq_bool();
	void check_id_in_read();
	void check_exp_in_range();
	/*read a new lexem*/
	void gl();
public:
	Poliz prog;
	Parser(const char* program) : scan(program), prog(1000) {}
	void Analyze();
};


