#pragma once
#include "lex.h"
#include "ident.h"
#include <queue>

extern Tabl_ident TID;
class Scanner
{
	static char * TW[]; /*table containing all keywords*/
	static type_of_lex words[]; /*the corresponding lexem type for the keyword*/
	static char * TD[];	/*table of language delimiters*/
	static type_of_lex dlms[]; /*The corresponding lexem type for the delimiters table*/
	ifstream file;
	char c, *buf;
	int buf_top, c_ind, float_flag, fl_list_ind = 0, count_symb, curr_lex_len, num_str, num_col;
	type_of_lex c_type;
	float float_num;
	//list<float> Float_type_list;
	double CheckExp(char *S, double N);
	double str2double(char *NumStr);
	void clear();
	int look(char *buf, char **list);
	typedef bool (Scanner::*State)(char c);
	State state = nullptr;
	/*reading any incoming character, return true always*/
	bool Start_state(char c);

	/*a state that reads a lexem, which is a word.
	returns false when the token is terminated, else true*/
	bool Read_word_state(char c);

	/*reading a const.
	returns false when the token is terminated, else true */
	bool Read_num_state(char c);

	/*the comment is a string, so we read before the line feed character.
	returns false when the token is terminated, else true*/
	bool Comment_state(char c);

	/*The first character in the lexem were =, > , <, so we analyze the lexeme:
	either an assignment operator is encountered, or an operator for logical comparisons.
	returns false when the token is terminated, else true*/
	bool Logic_oper_state(char c);
	bool Not_eq_state(char c);	/*state that adds an ! = operator or throws an exception. returns false when the token is terminated, else true*/
	bool other_div_state(char c);
	bool str_state(char c);
public:

	/* Requests the token and the printer for its name and its corresponding code in the format:
	"lex_name (num_type, ind) "*/
	void Print_Prog()
	{
		Lex l;
		while (l.get_type() != LEX_FIN)
		{
			get_lex(l);
			cout << l << endl;
		}
	}

	bool get_lex(Lex & lex); /*each time it issues the following lexem*/
	Scanner(const char * program);
	
};
