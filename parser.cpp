#include "parser.h"
void Parser::check_exp_in_range()
{
	while(c_type == LEX_ID || c_type == LEX_NUM)
	{
		if (c_type == LEX_ID)
		{
			check_id();
			int tmp;
			tmp = st_lex.top();
			st_lex.pop();
			if (tmp != LEX_INT)
				throw Wrong_type("range", curr_lex.get_str(), curr_lex.get_col());
			tmp_lexs.push_back(Lex(LEX_ID, c_val));
		}
		else 
			tmp_lexs.push_back(curr_lex);
		gl();
		if (c_type == LEX_PLUS || c_type == LEX_MINUS ||
			c_type == LEX_MULT || c_type == LEX_SLASH)
		{	
			tmp_lexs.push_back(Lex(c_type));
			gl();
		}
		else if (c_type != LEX_COMMA && c_type != LEX_RPAREN)
			throw Wrong_type("range", curr_lex.get_str(), curr_lex.get_col());
	}

	if(c_type != LEX_COMMA && c_type != LEX_RPAREN)
		throw Wrong_type("range", curr_lex.get_str(), curr_lex.get_col());
	
}

/*read a new lexem*/
void Parser::gl()
{
	scan.get_lex(curr_lex);
	c_type = curr_lex.get_type();
	c_val = curr_lex.get_value();
}

void Parser::Analyze()
{
	gl();
	count_if = 0;
	count_cycle = 0;
	Start();
	prog.print();
}
/*first reads the types of variables and their,  
and then begins to analyze the body of the program*/
void Parser::Start()
{
	ident_body();
	while (c_type == LEX_SEMICOLON || c_type == LEX_FIGUR_END)
	{
		gl();
		ident_body();
	}
	if (c_type != LEX_FIN)
	{
		throw Syntax_error(curr_lex, ";");
	}
}



/*reads variable names*/
void Parser::ident_variable()
{
	if (c_type != LEX_ID)
		throw curr_lex;
	else
	{
		st_int.push(c_val);
		gl();
		while (c_type == LEX_COMMA)
		{
			gl();
			if (c_type != LEX_ID)
				throw curr_lex;
			else
			{
				st_int.push(c_val);
				gl();
			}
		}
	}
}

void Parser::ident_body_in_figure()
{
	if (c_type == LEX_FIGUR_BEGIN)
	{
		if (!count_if && !count_cycle)
			throw curr_lex;
		gl();
		ident_body();
		while (c_type == LEX_SEMICOLON)
		{
			gl();
			ident_body();
		}
		if (c_type != LEX_FIGUR_END)
			throw Syntax_error(curr_lex, "}");
	}
	else
	{
		throw curr_lex;
	}
}

/*analyzes the body of the program*/
void Parser::ident_body()
{
	int pl0, pl1, pl2, pl3;
	if (c_type == LEX_ID)
	{
		TID[c_val].put_assign();
		check_id();
		prog.put_lex(Lex(POLIZ_ADDRESS, c_val, curr_lex.get_name()));
		gl();
		if (c_type == LEX_ASSIGN)
		{
			gl();
			ident_expression();
			eq_type();
			prog.put_lex(Lex(LEX_ASSIGN));
		}
		else
			throw Syntax_error(curr_lex, "=");
	}
	else if (c_type == LEX_IF)
	{
		gl();
		ident_expression();
		eq_bool();
		pl2 = prog.get_free ();
    	prog.blank();
    	prog.put_lex(Lex(POLIZ_FGO));
		count_if++;	
		ident_body();
		if (c_type == LEX_FIGUR_END)
			gl();
		count_if--;
		gl();
		if (c_type != LEX_ELSE && c_type != LEX_ELIF)
      		prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl2);
		while (c_type == LEX_ELSE || c_type == LEX_ELIF)
		{
			pl3 = prog.get_free();
			prog.blank();
      		prog.put_lex(Lex(POLIZ_GO));
			prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl2);
			if (c_type == LEX_ELIF)
			{
				count_if++;
				c_type = LEX_IF;
				ident_body();
				count_if--;
				gl();
				prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl3);
			}
			else
			{			
				gl();
				count_if++;
				ident_body();
				count_if--;
				gl();
				prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl3);
			}
		}
		ident_body();
	}
	
	else if (c_type == LEX_WHILE)
	{
		if (tmp_lst.size() != 0)
			tmp_lst.push(count_break);
		else if (count_break != 0)
			tmp_lst.push(count_break);

		count_break = 0;
		pl0=prog.get_free();
		pl_4_cont.push_front(pl0);
		gl();
		count_cycle++;
		ident_expression();
		eq_bool();
		pl1=prog.get_free(); 
		prog.blank();
    	prog.put_lex(Lex(POLIZ_FGO));
		ident_body();
		prog.put_lex(Lex(POLIZ_LABEL, pl0));
     	prog.put_lex(Lex(POLIZ_GO));
      	prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl1);
		int c = 0;
		while(!pl_4_break.empty() && c != count_break)
		{
			prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl_4_break.top());
			pl_4_break.pop();
			c++;
		}
		if (tmp_lst.size() != 0)
		{
			count_break = tmp_lst.top();
			tmp_lst.pop();
		}
		count_cycle--;
		pl_4_cont.pop_back();
		if (c_type == LEX_FIGUR_END && count_cycle != 0)
		{
			gl();
			ident_body();
		}
	}
	else if (c_type == LEX_FOR)
	{
	/*for i in range(E){S} in Poliz: label, !, addr i, i, 1, +, =,;, label to comp, !,addr  i, 0, =,;, i, E', <=, label to end, !F, S',  label to comparison, ! 
	for i in range(E1, E2){S} in Poliz:label, !, addr i, i, 1, +, =,;, label to comp, !, i, E1', =,;, i, E2', <=, label to end, !F, S',  label to comparison, !*/
		if (tmp_lst.size() != 0)
			tmp_lst.push(count_break);
		else if (count_break != 0)
			tmp_lst.push(count_break);
		count_break = 0;
		gl();
		if (c_type != LEX_ID)
			throw curr_lex;
		TID[c_val].put_assign();
		check_id();
		int pl5, tmp_val = c_val;
		pl0 = prog.get_free();
		prog.blank();
		prog.put_lex(Lex(POLIZ_GO));
		pl_4_cont.push_front(prog.get_free());
		pl5 = prog.get_free();
		prog.put_lex (Lex(POLIZ_ADDRESS, c_val, curr_lex.get_name()));
		prog.put_lex(Lex(LEX_ID, c_val, curr_lex.get_name()));
		prog.put_lex(Lex(LEX_NUM, 1));
		prog.put_lex(Lex(LEX_PLUS));
		prog.put_lex(Lex(LEX_ASSIGN));
		pl3 = prog.get_free();
		prog.blank();
		prog.put_lex(Lex(POLIZ_GO));
		prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl0);
		prog.put_lex(Lex(POLIZ_ADDRESS, c_val, curr_lex.get_name()));
		gl();
		if (c_type != LEX_IN)
			throw curr_lex;
		gl();
		if (c_type != LEX_RANGE)
			throw curr_lex;
		gl();
		if (c_type == LEX_LPAREN)
		{
			
			gl();
			check_exp_in_range();
			if (c_type == LEX_COMMA)
			{
				int i = 0;
				while (i != tmp_lexs.size())
				{
					prog.put_lex(tmp_lexs[i]);
					i++;
				}
				prog.put_lex(Lex(LEX_ASSIGN));
				prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl3);
				pl0 = prog.get_free();
				prog.put_lex(Lex(LEX_ID, tmp_val, curr_lex.get_name()));
				gl();
				count = 0;
				tmp_lexs.clear();
				check_exp_in_range();
				if(c_type == LEX_RPAREN)
					gl();
				else
					throw Syntax_error(curr_lex, ")");
			}
			else if (c_type == LEX_RPAREN)
			{
				prog.put_lex(Lex(LEX_NUM, 0));
				prog.put_lex(Lex(LEX_ASSIGN));
				prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl3);
				pl0 = prog.get_free();
				prog.put_lex(Lex(LEX_ID, tmp_val, curr_lex.get_name()));				
				gl();
			}
			else
				throw Syntax_error(curr_lex, ")");
		}
		else
			throw Syntax_error(curr_lex, "(");
		int i = 0;
		while (i != tmp_lexs.size())
		{
			prog.put_lex(tmp_lexs[i]);
			i++;
		}
		prog.put_lex(Lex(LEX_LEQ));
		pl1 = prog.get_free();
		prog.blank();
		prog.put_lex(Lex(POLIZ_FGO));
		count_cycle++;
		tmp_lexs.clear();
		ident_body();
		count_cycle--;
		prog.put_lex(Lex(POLIZ_LABEL, pl5));
     	prog.put_lex(Lex(POLIZ_GO));
     	prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl1);
     	int c = 0;
		while(!pl_4_break.empty() && c != count_break)
		{
			prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl_4_break.top());
			pl_4_break.pop();
			c++;
		}
		if (tmp_lst.size() != 0)
		{
			count_break = tmp_lst.top();
			tmp_lst.pop();
		}
		count_cycle--;
		pl_4_cont.pop_back();
		if (c_type == LEX_FIGUR_END && count_cycle != 0)
		{
			gl();
			ident_body();
		}
	}
	else if (c_type == LEX_BREAK || c_type == LEX_CONT)
	{
		if (!count_cycle)
			throw curr_lex;
		else if (c_type == LEX_CONT)
		{
			prog.put_lex(Lex(POLIZ_LABEL, pl_4_cont.back()));
     		prog.put_lex(Lex(POLIZ_GO));
			gl();
		}
		else if (c_type == LEX_BREAK)
		{
			pl_4_break.push(prog.get_free());
			prog.blank();
			prog.put_lex(Lex(POLIZ_GO));
			gl();
			count_break++;
		}
		
	}
	else if (c_type == LEX_READ)
	{
		gl();
		if (c_type == LEX_LPAREN)
		{
			gl();
			if (c_type == LEX_ID)
			{
          		TID[c_val].put_assign();
				check_id_in_read();
				prog.put_lex (Lex(POLIZ_ADDRESS, c_val, curr_lex.get_name()));
				gl();
			}
			else
				throw curr_lex;
			if (c_type != LEX_RPAREN)
				Syntax_error(curr_lex, ")");
			else
			{
				gl();
				prog.put_lex(Lex(LEX_READ));
			}
		}
		else
			throw Syntax_error(curr_lex, "(");
	}
	else if (c_type == LEX_PRINT)
	{
		gl();
		if (c_type == LEX_LPAREN)
		{
			gl();
			ident_expression();
			if (c_type == LEX_RPAREN)
			{
				gl();
				prog.put_lex(Lex(LEX_PRINT));
			}
			else
				throw Syntax_error(curr_lex, ")");
		}
		else
			throw Syntax_error(curr_lex, "(");
	}
	else if (c_type == LEX_INT)
	{
		gl();
		ident_variable();
		Check_declare(LEX_INT);
		if (c_type != LEX_SEMICOLON)
			throw Syntax_error(curr_lex, ";");
	}
	else if (c_type == LEX_BOOL)
	{
		gl();
		ident_variable();
		Check_declare(LEX_BOOL);
		if (c_type != LEX_SEMICOLON)
			throw Syntax_error(curr_lex, ";");
		
	}
	else if (c_type == LEX_FLOAT)
	{
		gl();
		ident_variable();
		Check_declare(LEX_FLOAT);
		if (c_type != LEX_SEMICOLON)
			throw Syntax_error(curr_lex, ";");
	}
	else if ( c_type == LEX_ELSE || c_type == LEX_ELIF)
	{
		throw Syntax_error(curr_lex, "if");
	}
	else if (c_type == LEX_FIGUR_END || c_type == LEX_FIN)
		return;
	else if (c_type == LEX_PRINTL)
	{
		gl();
		if (c_type == LEX_LPAREN)
		{
			gl();
			if (c_type == LEX_STR)
			{
				prog.put_lex(Lex(LEX_STR, 0, curr_lex.get_name()));
				gl();
			}
			if (c_type == LEX_RPAREN)
			{
				gl();
				prog.put_lex(Lex(LEX_PRINTL));
			}
			else
				throw Syntax_error(curr_lex, ")");
		}
		else
			throw Syntax_error(curr_lex, "(");
	}
	else
		ident_body_in_figure();
}

void Parser::ident_expression()
{
	expres1();
	if (c_type == LEX_EQ || c_type == LEX_LESS || c_type == LEX_MORE ||
		c_type == LEX_LEQ || c_type == LEX_GEQ || c_type == LEX_NEQ)
	{
		st_lex.push(c_type);
		gl();
		expres1();
		check_op();
	}
}

void Parser::expres1()
{
	expres2();
	while (c_type == LEX_PLUS || c_type == LEX_MINUS || c_type == LEX_OR)
	{
		st_lex.push(c_type);
		gl();
		expres2();
		check_op();
	}
}

void Parser::expres2()
{
	pars_oper();
	while (c_type == LEX_MULT || c_type == LEX_SLASH || c_type == LEX_AND)
	{
		st_lex.push(c_type);
		gl();
		pars_oper();
		check_op();
	}
}

void Parser::pars_oper()
{
	if (c_type == LEX_ID)
	{
		check_id();
		prog.put_lex(Lex(LEX_ID, c_val, curr_lex.get_name()));
		gl();
	}
	else if (c_type == LEX_NUM)
	{
		st_lex.push(LEX_INT);
		prog.put_lex(curr_lex);
		gl();
	}
	else if (c_type == LEX_FLOAT_NUM)
	{
		st_lex.push(LEX_FLOAT_NUM);
		prog.put_lex(curr_lex);
		gl();
	}
	else if (c_type == LEX_TRUE)
	{
		st_lex.push(LEX_BOOL);
		prog.put_lex(Lex(LEX_TRUE, 1, curr_lex.get_name()));
		gl();
	}
	else if (c_type == LEX_FALSE)
	{
		st_lex.push(LEX_BOOL);
		prog.put_lex(Lex(LEX_FALSE, 0, curr_lex.get_name()));
		gl();
	}
	else if (c_type == LEX_NOT)
	{
		gl();
		pars_oper();
		check_not();
	}
	else if (c_type == LEX_LPAREN)
	{
		gl();
		ident_expression();
		if (c_type == LEX_RPAREN)
			gl();
		else
			throw curr_lex;
	}
	else
		throw curr_lex;
}

void Parser::Check_declare(type_of_lex type)
{
	int i;
	while (!st_int.empty())
	{
		i = st_int.top();
		st_int.pop();
		if (TID[i].get_declare())
			throw Semantic_error(curr_lex, "twice");
		else
		{
			TID[i].put_declare();
			TID[i].put_type(type);
		}
	}
}

void Parser::check_id()
{
	if (TID[c_val].get_declare())
	{	
		if (TID[c_val].get_assign())
			st_lex.push(TID[c_val].get_type());
		else
			throw Semantic_error(curr_lex, "not initialized");
	}
	else
	{
		
		throw Semantic_error(curr_lex, "not declared");
	}
}

void Parser::check_op()
{
	type_of_lex t1, t2, op, t = LEX_INT, r = LEX_BOOL;
	t2 = st_lex.top();
	st_lex.pop();
	op = st_lex.top();
	st_lex.pop();
	t1 = st_lex.top();
	st_lex.pop();
	if (op == LEX_PLUS || op == LEX_MINUS || op == LEX_MULT || op == LEX_SLASH)
	{
		if (t1 == LEX_FLOAT || t2 == LEX_FLOAT)
			r = LEX_FLOAT;
		else 
			r = LEX_INT;
	}
	if (op == LEX_OR || op == LEX_AND)
		t = LEX_BOOL;
	if (t1 == LEX_FLOAT || t2 == LEX_FLOAT)
		st_lex.push(r);
	else if (t1 == t2 && t1 == t)
		st_lex.push(r);
	else
		throw  Wrong_type("operation", curr_lex.get_str(), curr_lex.get_col(), op);
	
	prog.put_lex(Lex(op)); 
}

void Parser::check_not()
{
	int tmp;
	tmp = st_lex.top();
	st_lex.pop();
	if (tmp != LEX_BOOL)
		throw  Wrong_type("not operation", curr_lex.get_str(), curr_lex.get_col());
	else
	{
		st_lex.push(LEX_BOOL);
		prog.put_lex(Lex(LEX_NOT));
	}
}

void Parser::eq_type()
{
	type_of_lex t1, t2;
	t1 = st_lex.top();
	st_lex.pop();
	t2 = st_lex.top();
	st_lex.pop();
	if (t1 != t2)
	{
		if (t1 == LEX_FLOAT && t2 == LEX_INT)
			throw  Wrong_type("assignment", curr_lex.get_str(), curr_lex.get_col());
	}
}

void Parser::eq_bool()
{
	
	if (st_lex.top() != LEX_BOOL)
	{	
		st_lex.pop();
		throw Wrong_type(curr_lex.get_str(), curr_lex.get_col(), LEX_BOOL);
	}
}

void Parser::check_id_in_read()
{
	if (!TID[c_val].get_declare())
		throw Semantic_error(curr_lex, "not declared");
}

