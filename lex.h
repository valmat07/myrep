#pragma once
#include <iostream>
#include <cstring>
#include <cstdio>
#include <fstream>
#include <cctype>
#include <cstdlib>
#include <exception>
#include <list>
#include <string>
#include <vector>
#include <stack>
#define MIN_LEX_LEN 255
using namespace std;
enum type_of_lex
{
	LEX_NULL, /*0*/
	LEX_AND, LEX_BOOL, LEX_ELSE, LEX_IF, LEX_FALSE, LEX_INT, LEX_FLOAT, LEX_ELIF,  /*8*/
	LEX_NOT, LEX_OR, LEX_READ, LEX_TRUE, LEX_WHILE, LEX_PRINT, LEX_FIGUR_BEGIN, LEX_FIGUR_END,  /*16*/
	LEX_FIN, /*17*/
	LEX_SEMICOLON, LEX_COMMA, LEX_COLON, LEX_ASSIGN, LEX_LPAREN, LEX_RPAREN, LEX_EQ, LEX_LESS, /*25*/
	LEX_MORE, LEX_PLUS, LEX_MINUS, LEX_MULT, LEX_SLASH, LEX_LEQ, LEX_NEQ, LEX_GEQ, /*33*/
	LEX_NUM, LEX_FOR, LEX_IN, LEX_RANGE, /*37*/
	LEX_ID, LEX_STRING, LEX_CONT, LEX_BREAK, LEX_FLOAT_NUM, LEX_PRINTL, LEX_STR,//44
	POLIZ_LABEL, 
 	POLIZ_ADDRESS, 
  	POLIZ_GO, 
  	POLIZ_FGO 
};
class Lex
{
	type_of_lex t_lex;
	int s_lex, c_lex, v_lex;
	float float_v_lex;
	char* name = nullptr;
public:
	Lex(type_of_lex t = LEX_NULL, int v = 0, const char * buf = "", int s = 1, int c = 1);
	Lex(const string tmp, type_of_lex t = LEX_NULL, float v = 0, const char * buf = "", int s = 1, int c = 1);
	type_of_lex get_type() {return t_lex;}
	int get_value() {return v_lex;}
	int get_str() {return s_lex;}
	int get_col() {return c_lex;}
	float get_float_value() {return float_v_lex;}
	char* get_name() {return name;}
	friend ostream& operator << (ostream &s, Lex l);
};
