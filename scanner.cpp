#include "scanner.h"

double Scanner::CheckExp(char *S, double N) 
{
	double Epart = 0.0;
	int CountMul = 0, i = 0;
	S++;
	if (*S == '+')
		Epart = 10;
	else if (*S == '-')
		Epart = 0.1;
	else
	{
		Epart = 10;
		CountMul = (double)*S - '0';
	}

	S++;
	while (*S != '\0') {
		CountMul = CountMul * 10 + (double)*S - '0';
		S++;
	}

	while (i != CountMul) {
		N *= Epart;
		i++;
	}
	return N;
}
double Scanner::str2double(char *NumStr)
{
	double NumDouble = 0.0, FracPart = 0.1, Epart = 0.0;
	//int CountMul = 0, i = 0;
	while (*NumStr != '.' && *NumStr != '\0')
	{
		if (*NumStr == 'E' || *NumStr == 'e') {
			NumDouble = CheckExp(NumStr, NumDouble);
			return NumDouble;
		}

		NumDouble = NumDouble * 10 + (double)*NumStr - '0';
		NumStr++;
	}

	if (*NumStr == '\0')
		return NumDouble;

	NumStr++;
	while (*NumStr != '\0')
	{
		if (*NumStr == 'E' || *NumStr == 'e') {
			NumDouble = CheckExp(NumStr, NumDouble);
			break;

		}
		NumDouble += ((double)*NumStr - '0') * FracPart;
		FracPart *= 0.1;
		NumStr++;

	}
	return NumDouble;
}
void Scanner::clear()
{
	buf_top = 0;
	for (int j = 0; j < count_symb; j++)
		buf[j] = '\0';
}
int Scanner::look( char *buf, char **list)
{
	int i = 0;
	buf[count_symb] = '\0';
	while (list[i])
	{
		if (!strcmp(buf, list[i]))
			return i;
		i++;
	}
	return 0;
}

Scanner::Scanner(const char * program)
{
	file.open(program);
	c = file.get();
	num_str = 1;
	num_col = 1;
	float_flag = 0;
	//clear();
}
/*reading any incoming character, return true always*/
bool Scanner::Start_state(char c)
{
	if (c == ' ' || c == '\r' || c == '\t')
		return true;
	else if (c == '\n')
	{
		num_str++;
		num_col = 0;
	}
	else if (isalpha(c))
	{
		clear();
		count_symb++;
		buf[buf_top++] = c;
		state = &Scanner::Read_word_state;
	}
	else if (isdigit(c))
	{
		clear();
		count_symb++;
		buf[buf_top++] = c;
		state = &Scanner::Read_num_state;
	}
	else if (c == '#')
	{
		state = &Scanner::Comment_state;
	}
	else if (c == '<' || c == '>' || c == '=')
	{
		clear();
		count_symb++;
		buf[buf_top++] = c;
		state = &Scanner::Logic_oper_state;
	}
	/*there may be a != or error*/
	else if (c == '!')
	{
		clear();
		count_symb++;
		buf[buf_top++] = c;
		state = &Scanner::Not_eq_state;
	}
	else if(c == '\"')
	{
		clear();
		count_symb++;
		state = &Scanner::str_state;
	}
	else
	{
		clear();
		count_symb++;
		buf[buf_top++] = c;
		state = &Scanner::other_div_state;
	}
	return true;
}

/*a state that reads a lexem, which is a word.
returns false when the token is terminated, else true*/
bool Scanner::Read_word_state(char c)
{
	if (isalpha(c) || isdigit(c))
	{
		count_symb++;
		buf[buf_top++] = c;
		return true;
	}
	/*met separator*/
	else if (c_ind = look(buf, TW))
	{
		c_type = words[c_ind];
		return false;
	}
	/*a word is a custom lexeme*/
	else
	{
		buf[count_symb] = '\0';
		c_ind = TID.put(buf);
 		c_type = LEX_ID;
		return false;
	}
	return true;
}

/*reading a const.
returns false when the token is terminated, else true */
bool  Scanner::Read_num_state(char c)
{
	if (c == '.')
	{
		if(float_flag)
			throw runtime_error("Incorret input");
		else
			float_flag = 1;
		buf[buf_top++] = c;
		count_symb++;
	}
	else if (isdigit(c))
	{
		buf[buf_top++] = c;
		count_symb++;
	}
	else
	{
		if (!float_flag)
		{
			c_type = LEX_NUM;
			buf[count_symb] = '\0';
			c_ind = atoi(buf);
		}
		else
		{
		
			c_ind = 0;
			buf[buf_top] = '\0';
			float_num = str2double(buf);
			/*)
			fl_list_ind++;
			Float_type_list.push_back(str2double(buf));
			cout << "!!!\n";*/
			c_type = LEX_FLOAT_NUM;
		}
		return false;
	}
	count_symb++;
	return true;
}

/*the comment is a string, so we read before the line feed character.
returns false when the token is terminated, else true*/
bool  Scanner::Comment_state(char c)
{
	if (c == '\n')
		state = &Scanner::Start_state;
	
	return true;
}

bool Scanner::str_state(char c)
{
	if (c == '\"')
	{
		state = &Scanner::Start_state;
		c_type = LEX_STR;
		buf[count_symb - 1] = '\0';
		Scanner::c = file.get();
		return false;
	}
	count_symb++;
	buf[buf_top++] = c;
	return true;
}

/*The first character in the lexem were =, > , <, so we analyze the lexeme:
either an assignment operator is encountered, or an operator for logical comparisons.
returns false when the token is terminated, else true*/
bool Scanner::Logic_oper_state(char c)
{

	if (c == '=')
	{
		buf[buf_top++] = c;
		count_symb++;
		c_ind = look(buf, TD);
		c_type = dlms[c_ind];
		Scanner::c = file.get();
		return false;
	}
	else
	{
		c_ind = look(buf, TD);
		c_type = dlms[c_ind];
		return false;
	}

}

/*state that adds an ! = operator or throws an exception.
returns false when the token is terminated, else true*/
bool Scanner::Not_eq_state(char c)
{
	if (c == '=')
	{
		buf[buf_top++] = c;
		count_symb++;
		c_ind = look(buf, TD);
		c_type = LEX_NEQ;
		Scanner::c = file.get();
		return false;
	}
	count_symb++;
	return true;
}

bool Scanner::other_div_state(char c)
{
	if (c_ind = look(buf, TD))
	{
		c_type = dlms[c_ind];
		return false;
	}
}


/*table containing all keywords*/
char * Scanner::TW[] =
{
	(char *)"", // 0 
	(char *)"and", // 1
	(char *)"bool", // 2
	(char *)"else", // 3
	(char *)"if", // 4
	(char *)"false", // 5
	(char *)"int", // 6
	(char *)"not", // 7
	(char *)"or", // 8
	(char *)"read", // 9
	(char *)"true", // 10
	(char *)"while", // 11
	(char *)"print", // 12
	(char *)"float", // 13
	(char *)"elif", // 14
	(char *)"string", // 15
	(char *)"continue", // 16
	(char *)"break", // 17
	(char *)"for", // 18
	(char *)"in", // 19
	(char *)"range",// 20
	(char *)"printl",
	(char *)"FIN", // 21
	nullptr
};

/*table of language delimiters*/
char * Scanner::TD[] =
{
	(char *)"", // 0 
	(char *)";", // 1
	(char *)",", // 2
	(char *)":", // 3
	(char *)"=", // 4
	(char *)"(", // 5
	(char *)")", // 6
	(char *)"==", // 7
	(char *)"<", // 8
	(char *)">", // 9
	(char *)"+", // 10
	(char *)"-", // 11
	(char *)"*", // 12
	(char *)"/", // 13
	(char *)"<=", // 14
	(char *)"!=", // 15
	(char *)">=", // 16
	(char *)"{", // 17
	(char *)"}", // 18
	nullptr
};
/*the corresponding lexem type for the keyword*/
type_of_lex Scanner::words[] = {
	LEX_NULL,
	LEX_AND,
	LEX_BOOL,
	LEX_ELSE,
	LEX_IF,
	LEX_FALSE,
	LEX_INT,
	LEX_NOT,
	LEX_OR,
	LEX_READ,
	LEX_TRUE,
	LEX_WHILE,
	LEX_PRINT,
	LEX_FLOAT,
	LEX_ELIF,
	LEX_STRING,
	LEX_CONT,
	LEX_BREAK,
	LEX_FOR,
	LEX_IN,
	LEX_RANGE,
	LEX_PRINTL,
	LEX_FIN,
	LEX_NULL
};

/*The corresponding lexem type for the delimiters table*/
type_of_lex Scanner::dlms[] = {
	LEX_NULL,
	LEX_SEMICOLON,
	LEX_COMMA,
	LEX_COLON,
	LEX_ASSIGN,
	LEX_LPAREN,
	LEX_RPAREN,
	LEX_EQ,
	LEX_LESS,
	LEX_MORE,
	LEX_PLUS,
	LEX_MINUS,
	LEX_MULT,
	LEX_SLASH,
	LEX_LEQ,
	LEX_NEQ,
	LEX_GEQ,
	LEX_FIGUR_BEGIN,
	LEX_FIGUR_END,
	LEX_NULL
};

/*each time it issues the following lexem.
with each new symbol through the pointer to the state function,
we navigate through the states of the automaton, analyzing the incoming symbol
*/
bool Scanner::get_lex(Lex & lex)
{
	state = &Scanner::Start_state;
	count_symb = 0;
	curr_lex_len = MIN_LEX_LEN;
	buf = new char [curr_lex_len];
	while (!file.eof())
	{

		if (!(this->*state)(c))
		{
			if (!float_flag)
			{
 				lex = Lex(c_type, c_ind, buf, num_str, num_col - count_symb);
			}
			else
			{ 
				lex = Lex("float", c_type, float_num, buf, num_str, num_col - count_symb);
				float_flag = 0;
			}
			delete [] buf;
			return true;
		}
		else
		{
			c = file.get();
			num_col++;
			if (count_symb > curr_lex_len)
			{
				curr_lex_len += MIN_LEX_LEN;
				buf = (char *)realloc(buf, curr_lex_len);

			}		
		
		}
	}
	strncpy(buf, "FIN", 3);
	lex = Lex(LEX_FIN, words[look(buf, TW)]);
	return true;
}
