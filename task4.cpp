#include "lex.h"
#include "scanner.h"
#include "ident.h"
#include "parser.h"
#include "Error_lex.h"
#include "exec.h"
using namespace std;

Tabl_ident TID(100);

int main()
{
	try 
	{
		Parser sc("max.txt");
		sc.Analyze();
		Executer ex;
		ex.execute(sc.prog);
		
	}
	catch (Semantic_error & er)
	{
		cout << er.what() << endl;
	}
	catch(Syntax_error & er)
	{
		cout << er.what() << endl;
	}
	catch (Wrong_type & er)
	{
		cout << er.what() << endl;
	}
	catch (Lex & er) {
		cout << "in line " << er.get_str() << ":" << er.get_col() << " ";
		cout << "unexpected lexem " << er.get_name() << endl;
	}
	catch(runtime_error & e)
	{
		cout << e.what() << endl;
	}
	cout << "END\n";
	return 0;
}