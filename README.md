# Task4
Задание: интерпретатор модельного языка программирования
Добавлены три примера программ: 
1)fib.txt - запрашивает номер число фибоначи и выводит его 
2)max.txt - запрашивает три числа и выводит максимальное из них 
3)sum.txt - запрашивает два числа и выводит двойного сумму ряда (i+j)	
##Модельный язык: 
* типы:
    * int 
    * bool 
    * float
    * list(возможно)
* циклы:
    * while 
    * for <var> in range(var|const)
    * <cycle_name> { <cycle_body> }
    * continue - продолжение цикла
    * break - досрочный выход из цикла
* операции:
    * = - присваивание
    * +, -, * - соответствующие операции
    * ==, >, <, <= , >= - операции сравнения
    * and, or - логические операции
* Условия: if {<body_if>} <else|elif> {}, elif  - аналогично конструкции else if 
Символ ; выступает разделителем 
###Грамматика:
* ident_type -> [int|bool] ident_variable {;ident_type}
* ident_variable -> I{,I};
* ident_body_in_figure -> '{' ident_body {;ident_body} '}'

*  ident_body -> I = ident_expression|if ident_expression ident_body [else|elif] ident_body| read(I) | print(I) | while (ident_expression) <count_cycle++> ident_body <count_cycle--> | for ident_expression in range([I| I, I]) <count_cycle++> ident_body <count_cycle-->| ident_body_in_figure | <if (count_cycle)> break {;ident_body} | <if (count_cycle)> continue {;ident_body} 	

* ident_expression -> expres1 [== | <| > |!= |>= | <= ] expres1 | expres1
* expres1 -> expres2{[+ | - | or]expres2}
* expres2 -> pars_oper {[* | / | and] pars_oper}
* pars_oper -> I | N | L | not pars_oper | (ident_expression)
* L -> true | false
* I -> C | IC | IR
* N -> R | NR
* C -> a|b ..... A| ......|Z
* R -> 0|1|2 ..... |9

	
* Реализованные типы: 
    * bool
    * int
    * float
####Содержание проекта:
* lex.cpp, lex.h - описание класса для лексему
* Scanner.cpp, Scanner.h - содержит обработчик программы и метод get_lex(Lex&) создаюзию лексему
* Parser.cpp, Parser.h - обработка программы по одной лексеме использую функцию get_lex()
* Error_lex.cpp, Error_lex.h - содрежит классы ошибок для корректной их обработки в программе. 
* сборка проекта - make task4
* запуск - make run
На данный момент реализован ЛА, СинА, СемА, + свой класс ошибок(class Not_dec_or_twice - не объявлена перменная, или повторяется дважды;
														class Wrong_type - не верный тип выражения
														class Syntax_error - не верный синтаксис)
